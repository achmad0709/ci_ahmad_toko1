<?php defined('BASEPATH') OR exit('No direct script acces allowed');

class Supplier extends CI_controller {

	
	
	
	public function __construct()
	
	{
		
		parent::__construct();
		//load model terkait
		$this->load->model("supplier_model");
		
		//cek sesi login
		$user_login =$this->session->userdata();
		if (count($user_login)<=1){
			redirect("auth/index", "refresh");
		}
		
	}
	
	public function index()
	
	{
		$this->listsupplier();
	}
	
	public function listsupplier()
	
	{
		$data['data_supplier'] = $this->supplier_model->tampilDataSupplier();
		$data['content']		= 'forms/list_supplier';
		$this->load->view('home2', $data);
	}


	public function Input()
	{
		$data['data_supplier'] = $this->supplier_model->tampilDataSupplier();
		$data['content']		= 'forms/InputSupplier';
		
		/*if (!empty($_REQUEST)) {
			$m_supplier = $this->supplier_model;
			$m_supplier->save();
			redirect("supplier/index", "refresh"); 
		}*/

		$validation = $this->form_validation;
		$validation->set_rules($this->supplier_model->rules());
		
		if ($validation->run()) {
			$this->supplier_model->save();
			$this->session->set_flashdata('info', '<div style="color: green">Simpan Data Berhasil !</div>');
			redirect("supplier/index", "refresh");
		}
		
		
		$this->load->view('home2', $data);
	}
	
	   public function detailsupplier($kode_supplier)
	   {
			$data['detail_supplier']= $this->supplier_model->detailsupplier($kode_supplier);
			$this->load->view('detail_supplier', $data);   
	   }
	   public function Editsupplier($kode_supplier)
	{
	
		$data['detail_supplier'] = $this->supplier_model->detailsupplier($kode_supplier);
		$data['content']		= 'forms/Editsupplier';
		
		/*if (!empty($_REQUEST)) {
			$m_supplier = $this->supplier_model;
			$m_supplier->update($kode_supplier);
			redirect("supplier/index", "refresh"); 
		}*/

		$validation = $this->form_validation;
		$validation->set_rules($this->supplier_model->rules());
		
		if ($validation->run()) {
			$this->supplier_model->update($kode_supplier);
			$this->session->set_flashdata('info', '<div style="color: green">Simpan Data Berhasil !</div>');
			redirect("supplier/index", "refresh");
		}
		
		$this->load->view('home2', $data);
	}
	
	public function deletesupplier($kode_supplier)
	{
		$m_supplier = $this->supplier_model;
		$m_supplier->delete($kode_supplier);
		redirect("supplier/index", "refresh");
		
	}
	
	
	
	
	
	
}
